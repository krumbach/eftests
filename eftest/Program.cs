﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;

namespace eftest
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");

			string connectionString = ConfigurationManager.ConnectionStrings["MyContext"].ToString();

			using (var connection = new MySqlConnection(connectionString))
			{
				using (var context = new MyContext(connection, false))
				{
					var regions = context.Regions.ToList();
					foreach (var region in regions)
					{
						Console.WriteLine(region.Id + " " + region.Code + " " + region.Name);
					}

					/*context.Regions.Add(new Region
					{
						Name = "ted",
						Code = "TSK"
					});

					context.SaveChanges();*/

					var count = context.PatientStats.Count();

					Console.WriteLine(count);


					foreach (var stat in context.PatientStats)
					{
						Console.WriteLine(stat.Id + " " + stat.PatientCount);
					}
				}
			}
		}
	}
}
