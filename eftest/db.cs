﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using MySql.Data.Entity;

[DbConfigurationType(typeof(MySqlEFConfiguration))]
public class MyContext : DbContext
{
	public MyContext(DbConnection existingConnection, bool contextOwnsConnection)
	  : base(existingConnection, contextOwnsConnection)
	{

	}

	public virtual DbSet<Region> Regions { get; set; }
	public virtual DbSet<PatientStats> PatientStats { get; set; }
}

public class Region
{
	public int Id { get; set; }
	public string Name { get; set; }
	public string Code { get; set; }
}

[Table("patient_stats")]
public class PatientStats
{
	public int Id { get; set; }

	[Column("patient_count")]
	public string PatientCount { get; set; }

	[Column("field_count")]
	public string FieldCount { get; set; }

	[Column("plan_count")]
	public string PlanCount { get; set; }
}